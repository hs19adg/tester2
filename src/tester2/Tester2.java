/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tester2;

/**
 *
 * @author harshil.janu.solanki
 */
import java.util.Scanner;

public class Tester2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        StockItem1 product1 = new StockItem1("Backed beans",65,15);
        product1.printReport();
        StockItem1 product2 = new StockItem1("Eggs",175,10);
        product2.printReport();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Price");
        Double price = scanner.nextDouble();
        StockItem1 product3 = new StockItem1("Bread",price,7);
        product3.printReport();
        
        product1.doDeliver(10);
        product1.printReport();
        product2.doSale(87.5);
        product2.printReport();
        
        
        
    }
    
}
