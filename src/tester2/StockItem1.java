/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tester2;

/**
 *
 * @author harshil.janu.solanki
 */
public class StockItem1 {
    
    private String Description;
    private int Level;
    private double Price;
    
    
    public StockItem1(String desc,double price,int level)
    {
        this.Description = desc;
        this.Price = price;
        this.Level = level;
        
        //sellByDate=new Date(1, 10, 18);
        
    }

    StockItem1() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String toString()
    {
        return this.getDescription();
        
    }
    
    public void printReport()
    {
         System.out.println(";*******Stock Report ********** *");
         System.out.println(";Item : "+this.getDescription()+"");
         
	System.out.println("Price: "+this.getPrice()+"p*");
	System.out.println("Stock level : "+this.getLevel()+"*");
        //System.out.println("Sell by : "+this.getSellByDate()+"*");
        //System.out.println("******************************");

    }
    
    public void doDeliver(int amount)
    {
        this.Level = this.Level + amount;
    }
    public boolean doSale(double amount)
    {
        if(this.Price >= amount)
        {
            this.Price = this.Price - amount;
            return true;
        }
        return false;
    }

    /**
     * @return the Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description the Description to set
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return the Level
     */
    public int getLevel() {
        return Level;
    }

    /**
     * @param Level the Level to set
     */
    public void setLevel(int Level) {
        this.Level = Level;
    }

    /**
     * @return the Price
     */
    public double getPrice() {
        return Price;
    }

    /**
     * @param Price the Price to set
     */
    public void setPrice(double Price) {
        this.Price = Price;
    }
    
}
